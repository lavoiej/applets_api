====
ApplETS API source code
====

Welcome to the ApplETS API source code repository. First of all, this is the
second API project of the club. The first was built with another framework and
is now deprecated. This new project aims to easily share source code with the
community and to gather contributors from everywhere. For that to be possible,
this project is under the GPLv3 licence.

Goal
____

The goal of the API is to offer to the ApplETS club of the ÉTS in Montréal a
centralized solution for all back end processing of the services offered.

Language
----
The main language in this repository is English. There are some members in
the club that speaks only english. Please, communicate in project in English
for those members. Anyway, to gather any contributors, the english language
has become the universal language in computer science.

Documentation
----
The format used for documentation is reStructuredText. Please use this notation
to write documents for the wiki.

Contribution
----
See the `contribution page <https://bitbucket.org/clubapplets/applets_api/wiki/Contribution>` in the wiki.
